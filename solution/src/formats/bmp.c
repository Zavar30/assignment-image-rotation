//
// Created by Ярослав on 30.10.2021.
//
#include "../../include/formats/bmp.h"
#include "../include/image/image.h"
#include <stdlib.h>

#define BFTYPE 0x4D42
#define BOFFBITS 54
#define BISIZE 40

uint32_t calcOffset(uint64_t width) { return (4 - ((3 * width) % 4)) % 4; }

struct maybeImage from_bmp(FILE *in) {
  BMPHeader bmpHeader;
  if (1 != read_bmp_header(in, &bmpHeader)) {
    return (struct maybeImage){.img = NULL, .status = READ_INVALID_HEADER};
  }

  Image img = createEmptyImage(bmpHeader.biWidth, bmpHeader.biHeight);
  uint32_t offset = calcOffset(img.width);
  for (uint32_t i = 0; i < img.height; i++) {
      fread(img.data + i * img.width, sizeof(struct pixel), img.width, in);
      fseek(in, offset, SEEK_CUR);
  }
  return (struct maybeImage){.img = &img, .status = READ_OK};
}

enum write_status to_bmp(FILE *out, Image const *img) {

  if (!out) {
    return INVALID_FILE;
  }

  write_bmp_header(out, img->width, img->height);

  uint32_t offset = calcOffset(img->width);
  char forOffset = 0x00;

  for (uint32_t i = 0; i < img->height; i++) {
      fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
      for (uint32_t j = 0; j < offset; j++) {
        fwrite(&forOffset, sizeof(char), 1, out);
      }
  }
  return WRITE_OK;
}

size_t read_bmp_header(FILE *in, BMPHeader *bmpHeader) {
  return fread(bmpHeader, sizeof(BMPHeader), 1, in);
}

void write_bmp_header(FILE *out, uint64_t width, uint64_t height) {
  BMPHeader bmpHeader;
  bmpHeader.bfType = BFTYPE;
  bmpHeader.bfileSize = height * width * 3 + BOFFBITS;
  bmpHeader.bfReserved = 0;
  bmpHeader.bOffBits = BOFFBITS;
  bmpHeader.biSize = BISIZE;
  bmpHeader.biWidth = width;
  bmpHeader.biHeight = height;
  bmpHeader.biPlanes = 1;
  bmpHeader.biBitCount = 24;
  bmpHeader.biCompression = 0;
  bmpHeader.biSizeImage = height * width * 3;
  bmpHeader.biXPelsPerMeter = 0;
  bmpHeader.biYPelsPerMeter = 0;
  bmpHeader.biClrUsed = 0;
  bmpHeader.biClrImportant = 0;

  fwrite(&bmpHeader, sizeof(BMPHeader), 1, out);
}
