//
// Created by Ярослав on 16.12.2021.
//
#include "../../include/image/image.h"
#include <stdlib.h>

Image createEmptyImage(uint64_t width, uint64_t height) {
  Image img;

  img.height = height;
  img.width = width;

  img.data = (struct pixel *)malloc(sizeof(struct pixel) * width * height);

  return img;
}
