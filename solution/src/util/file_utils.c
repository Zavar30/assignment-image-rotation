//
// Created by Ярослав on 30.10.2021.
//
#include "../../include/util/file_utils.h"
#include "../../include/formats/bmp.h"
#include "../include/image/image.h"
#include <stdlib.h>

struct maybeImage read_image(FILE **file, const char *path,
                             struct maybeImage (*from_format)(FILE *in)) {
  *file = fopen(path, "rb");
  return from_format(*file);
}

enum write_status
write_image(FILE **file, const char *path, Image *output,
            enum write_status (*to_format)(FILE *out, Image const *img)) {
  *file = fopen(path, "wb");
  return to_format(*file, output);
}
