#include "../include/formats/bmp.h"
#include "../include/transform/rotate.h"
#include "../include/util/file_utils.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  if (argc != 3) {
    printf("Введите путь до входящего и исходящего файлов\n");
    exit(1);
  }

  FILE *inputFile = NULL;
  FILE *outputFile = NULL;

  struct maybeImage bruhImage = read_image(&inputFile, argv[1], from_bmp);

  switch (bruhImage.status) {
  case READ_FAILED:
    printf("Ошибка чтения входящего файла\n");
    goto ex;
  case READ_INVALID_SIGNATURE:
    printf("Сигнатура bmp повреждена\n");
    goto ex;
  case READ_INVALID_HEADER:
    printf("Заголовок bmp поврежден\n");
    goto ex;
  case READ_OK:
    break;
  }

  Image inputImage = *bruhImage.img;

  if (fclose(inputFile)) {
    printf("Ошибка закрытия файла\n");
    goto cleanInput;
  }

  Image outputImage = rotate(&inputImage);

  switch (write_image(&outputFile, argv[2], &outputImage, to_bmp)) {
  case WRITE_FAILED:
    printf("Ошибка записи в исходящий файл\n");
    goto clean;
  case INVALID_FILE:
    printf("Косячный исходящий файл\n");
    goto clean;
  case WRITE_OK:
    break;
  }

  if (fclose(outputFile)) {
    printf("Ошибка закрытия файла\n");
    goto clean;
  }

clean:
  free(outputImage.data);
cleanInput:
  free(inputImage.data);
ex:
  return 0;
}
