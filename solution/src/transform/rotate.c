//
// Created by Ярослав on 31.10.2021.
//

#define PI 3.1415926

#include "../../include/transform/rotate.h"
#include "../include/image/image.h"
#include <math.h>
#include <stdlib.h>

// j = x, i = y, a - angle
// x'= x*cos(a) - y*sin(a)
// y'= x*sin(a) - y*cos(a)
// double angle = 1.0 * 90 * PI / 180;
// xr = (uint32_t)(j * 0 - i * 1);
// yr = (uint32_t)(j * 1 - i * 0);
// xr = -i + imgRot->width - 1;
// imgRot->width * yr + xr
Image rotate(Image const *source) {
  Image imgRot = createEmptyImage(source->height, source->width);
  for (uint32_t i = 0; i < imgRot.width; i++) {
    for (uint32_t j = 0; j < imgRot.height; j++) {
      // printf("%d(%d; %d)(%d; %d) - r:%d, g:%d, b:%d\n", width * i + j, j, i,
      // xr, yr, source->data[width * i + j].r, source->data[width * i + j].g,
      // source->data[width * i + j].b);
      imgRot.data[imgRot.width * j - i + imgRot.width - 1] =
          source->data[source->width * i + j];
    }
  }
  return imgRot;
}
