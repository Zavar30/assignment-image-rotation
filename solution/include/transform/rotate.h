//
// Created by Ярослав on 30.10.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

#include "../image/image.h"

Image rotate(Image const *source);

#endif // ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
