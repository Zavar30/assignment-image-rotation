//
// Created by Ярослав on 30.10.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "../image/image.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

typedef struct __attribute__((packed)) {
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
} BMPHeader;

enum read_status {
  READ_OK = 0,
  READ_FAILED,
  READ_INVALID_SIGNATURE,
  READ_INVALID_HEADER
};

struct maybeImage {
  Image *img;
  enum read_status status;
};

struct maybeImage from_bmp(FILE *in);

enum write_status { WRITE_OK = 0, WRITE_FAILED, INVALID_FILE };

enum write_status to_bmp(FILE *out, Image const *img);

uint32_t calcOffset(uint64_t width);

size_t read_bmp_header(FILE *in, BMPHeader *bmpHeader);

void write_bmp_header(FILE *out, uint64_t width, uint64_t height);

#endif // ASSIGNMENT_IMAGE_ROTATION_BMP_H
