//
// Created by Ярослав on 30.10.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <inttypes.h>

typedef struct {
  uint64_t width, height;
  struct pixel *data;
} Image;

struct pixel {
  uint8_t b, g, r;
};

Image createEmptyImage(uint64_t width, uint64_t height);

#endif // ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
