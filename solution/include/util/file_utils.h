//
// Created by Ярослав on 30.10.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H

#include "../../include/formats/bmp.h"
#include "../image/image.h"
#include <stdio.h>

struct maybeImage read_image(FILE **file, const char *path,
                             struct maybeImage (*from_format)(FILE *in));

enum write_status write_image(FILE **file, const char *path, Image *output,
                              enum write_status (*to_format)(FILE *out,
                                                             Image const *img));

#endif // ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H
